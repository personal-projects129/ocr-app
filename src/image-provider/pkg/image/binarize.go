package image

import (
	"image"
	"image/color"
)

const (
	uInt16ToUInt8Scale = float64(65536) / float64(255)
)

// Binarize binarize image
func Binarize(img image.Image, threshold uint8) image.Image {

	gray := GrayScale(img)
	width, height := gray.Bounds().Dx(), gray.Bounds().Dy()
	binarized := image.NewGray(image.Rect(0, 0, width, height))
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			c, _, _, _ := gray.At(x, y).RGBA()
			if c > uint32(float64(threshold)*uInt16ToUInt8Scale) {
				binarized.Set(x, y, color.Gray{
					Y: 255,
				})
			} else {
				binarized.Set(x, y, color.Gray{
					Y: 0,
				})
			}

		}
	}
	return binarized

}
