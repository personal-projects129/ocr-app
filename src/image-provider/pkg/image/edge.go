package image

import "image"

// EdgeDetect find edges
func EdgeDetect(img image.Image) image.Image {

	kernel := Kernel([][]float64{
		{-1, -1, -1},
		{-1, 8, -1},
		{-1, -1, -1},
	})

	edged, _ := Convolve(GrayScale(img), kernel)

	return edged
}
