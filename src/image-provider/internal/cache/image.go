package cache

import (
	"context"
	"log"
	"ocrapp/src/image-provider/internal/conf"
	"ocrapp/src/image-provider/internal/repository"
	"ocrapp/src/image-provider/internal/service"
	"strings"
	"sync"

	"github.com/golang/groupcache"
)

var (
	imageGroupOnce sync.Once
	imageGroup     *groupcache.Group
)

const (
	imageGroupName = "image"
)

// GetImageGroup get  image group
func GetImageGroup() *groupcache.Group {
	imageGroupOnce.Do(func() {
		config := conf.GetConfig()
		imageGroup = groupcache.NewGroup(imageGroupName, config.CacheBytes, groupcache.GetterFunc(getterFunc))
	})
	return imageGroup
}

func getterFunc(ctx context.Context, key string, dest groupcache.Sink) error {
	log.Println("Executed Getter Func for key: " + key)
	splitted := strings.Split(key, "-")
	if len(splitted) == 1 {
		data, err := repository.NewMinioImageRepository().GetImage(key)
		dest.SetBytes(data)
		return err
	}
	raw := []byte{}

	GetImageGroup().Get(ctx, strings.Join(splitted[:len(splitted)-1], "-"), groupcache.AllocatingByteSliceSink(&raw))
	processed, err := service.NewImageProcessorService(splitted...).DoOperation(raw, splitted[len(splitted)-1])
	if err != nil {
		return err
	}
	dest.SetBytes(processed)
	return nil
}
