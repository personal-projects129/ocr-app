package handler

import "net/http"

// HealthCheck implement healthc heck
func HealthCheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(200)
}
