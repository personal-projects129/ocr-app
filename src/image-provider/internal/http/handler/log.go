package handler

import (
	"log"
	"net/http"
)

// AddLogging adds logging to handler
func AddLogging(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		go func() {
			log.Printf("%s %s %s", req.RemoteAddr, req.Method, req.RequestURI)
		}()
		handler.ServeHTTP(res, req)
	})
}
