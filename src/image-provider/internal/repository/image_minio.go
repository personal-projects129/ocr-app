package repository

import (
	"io/ioutil"

	"ocrapp/src/image-provider/internal/conf"

	"github.com/minio/minio-go/v7"
)

// MinioImageRepository minio implementation of ImageRepository
type MinioImageRepository struct {
	client *minio.Client
}

// NewMinioImageRepository retursn new minio image repository
func NewMinioImageRepository() *MinioImageRepository {
	return &MinioImageRepository{
		client: getMinioClient(),
	}
}

// GetImage returns image from minio service
func (repo *MinioImageRepository) GetImage(path string) ([]byte, error) {
	obj, err := repo.client.GetObject(minioCtx, conf.GetConfig().Minio.Bucket, path, minio.GetObjectOptions{})
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(obj)
}
