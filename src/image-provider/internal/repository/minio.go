package repository

import (
	"log"
	"ocrapp/src/image-provider/internal/conf"
	"sync"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"golang.org/x/net/context"
)

var (
	minioClient *minio.Client
	minioCtx    = context.Background()
	minioOnce   sync.Once
)

func getMinioClient() *minio.Client {
	minioOnce.Do(func() {
		config := conf.GetConfig()
		newClient, err := minio.New(config.Minio.Host, &minio.Options{
			Creds:  credentials.NewStaticV4(config.Minio.AccessID, config.Minio.SecretAccessKey, ""),
			Secure: false,
		})
		if err != nil {
			log.Fatal("Failed to initialize minio client: " + err.Error())
		}
		minioClient = newClient
	})
	return minioClient
}
