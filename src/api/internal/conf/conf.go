package conf

import (
	"io/ioutil"

	"log"

	"gopkg.in/yaml.v2"
)

// Config - Api Configuration
type Config struct {
	ListenAddress string `yaml:"listenAddress"`
	HMACSecret    string `yaml:"hmacSecret"`
	AuthConfig    struct {
		Google struct {
			ClientID     string `yaml:"clientID"`
			ClientSecret string `yaml:"clientSecret"`
			RedirectURI  struct {
				Android struct {
					HTTP     string `yaml:"http"`
					DeepLink string `yaml:"deepLink"`
				} `yaml:"android"`
				Web struct {
					HTTP string `yaml:"http"`
				} `yaml:"web"`
			} `yaml:"redirectURI"`
			Scope []string `yaml:"scope"`
		} `yaml:"google"`
	} `yaml:"auth"`
	Redis struct {
		Host     string `yaml:"host"`
		Port     string `yaml:"port"`
		Password string `yaml:"password"`
	} `yaml:"redis"`
	MariaDB struct {
		Host     string `yaml:"host"`
		Port     string `yaml:"port"`
		Database string `yaml:"database"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
	} `yaml:"mariadb"`
	Minio struct {
		Host            string `yaml:"host"`
		AccessID        string `yaml:"accessID"`
		SecretAccessKey string `yaml:"secretAccessKey"`
		Bucket          string `yaml:"bucket"`
	} `yaml:"minio"`
	NSQ struct {
		Host string `yaml:"host"`
	} `yaml:"nsq"`
}

var (
	instance *Config
)

// LoadConfig - Load Api Configuration
func LoadConfig(path string) *Config {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("Failed to load configuration file: %s\n", path)
		log.Fatal(err)
	}

	config := Config{}
	err = yaml.Unmarshal([]byte(data), &config)

	if err != nil {
		log.Printf("Failed to Serialize configuration file should be a valid yaml: \n%s", string(data))
		log.Fatal(err)
	}

	log.Printf("Loaded Configuration file: %s\n%s\n", path, string(data))
	instance = &config

	return instance
}

// GetConfig - Return Loaded Configuration
func GetConfig() *Config {
	return instance
}
