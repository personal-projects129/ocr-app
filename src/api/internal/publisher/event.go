package publisher

import "ocrapp/src/api/internal/model"

// EventPublisher publish events
type EventPublisher interface {
	PublishJobEvent(userID string, event model.JobEvent) error
}
