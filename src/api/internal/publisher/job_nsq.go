package publisher

import (
	"encoding/json"
	"ocrapp/src/api/internal/model"
)

// JobNSQPublisher NSQ implementation of JobPublisher
type JobNSQPublisher struct {
	topic string
}

const (
	topic = "ocrapp_job"
)

// NewJobNSQPublisher returns new JobNSQPublisher
func NewJobNSQPublisher() *JobNSQPublisher {
	return &JobNSQPublisher{
		topic: topic,
	}
}

// PublishJob publish job to nsq
func (publisher *JobNSQPublisher) PublishJob(job model.Job) error {
	producer := newNSQProducer()
	defer producer.Stop()

	payload, err := json.Marshal(job)
	if err != nil {
		return err
	}

	return producer.Publish(publisher.topic, payload)
}
