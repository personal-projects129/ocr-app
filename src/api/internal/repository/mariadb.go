package repository

import (
	"fmt"
	"ocrapp/src/api/internal/conf"
	"sync"
)

var (
	dsn         string
	mariaDBOnce sync.Once
)

const (
	mariadbDriverName = "mysql"
)

func getMariaDBDSN() string {
	mariaDBOnce.Do(func() {
		config := conf.GetConfig().MariaDB
		dsn = fmt.Sprintf("%s:%s@tcp(%s)/%s", config.Username, config.Password, config.Host, config.Database)
	})

	return dsn
}
