package repository

import (
	"bytes"
	"io/ioutil"
	"log"
	"ocrapp/src/api/internal/conf"

	"github.com/minio/minio-go/v7"
)

// ImageMinioRepository minio implementation of image store
type ImageMinioRepository struct {
	client *minio.Client
}

// NewImageMinioRepository creates new ImageMinioRepository instance
func NewImageMinioRepository() *ImageMinioRepository {
	return &ImageMinioRepository{
		client: getMinioClient(),
	}
}

// StoreImage stores image to minio storage
func (repo *ImageMinioRepository) StoreImage(path string, data []byte, contentType string) error {
	repo.createBucketIfNotExists()

	reader := bytes.NewReader(data)
	_, err := repo.client.PutObject(minioCtx, conf.GetConfig().Minio.Bucket, path, reader, reader.Size(), minio.PutObjectOptions{
		ContentType: contentType,
	})
	return err
}

// GetImage gets image from minio storage
func (repo *ImageMinioRepository) GetImage(path string) ([]byte, error) {
	repo.createBucketIfNotExists()
	obj, err := repo.client.GetObject(minioCtx, conf.GetConfig().Minio.Bucket, path, minio.GetObjectOptions{})
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(obj)
}

// DeleteImage deletes image from minio storage
func (repo *ImageMinioRepository) DeleteImage(path string) ([]byte, error) {
	repo.createBucketIfNotExists()

	data, err := repo.GetImage(path)
	if err != nil {
		return nil, err
	}

	return data, repo.client.RemoveObject(minioCtx, conf.GetConfig().Minio.Bucket, path, minio.RemoveObjectOptions{})
}

// Creates bucket if the bucket does not exist
func (repo *ImageMinioRepository) createBucketIfNotExists() {
	exists, _ := repo.client.BucketExists(minioCtx, conf.GetConfig().Minio.Bucket)
	if !exists {
		err := repo.client.MakeBucket(minioCtx, conf.GetConfig().Minio.Bucket, minio.MakeBucketOptions{})
		if err != nil {
			log.Fatal("Failed to create bucket:" + err.Error())
		}
	}
}
