package repository

import (
	"ocrapp/src/api/internal/model"
)

// SessionRepository interface
type SessionRepository interface {
	StoreSession(key string, sess model.Session) error
	GetSession(key string) (model.Session, error)
	DestroySession(key string) error
	StoreBearer(key string, bearer model.BearerToken) error
	GetBearer(key string) (model.BearerToken, error)
}
