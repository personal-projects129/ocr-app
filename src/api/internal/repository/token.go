package repository

// TokenRepository token repository
type TokenRepository interface {
	GetToken(params map[string]string) (string, error)
}
