package repository

import (
	"context"
	"sync"

	"ocrapp/src/api/internal/conf"

	"github.com/go-redis/redis/v8"
)

var (
	redisClient *redis.Client
	redisOnce   sync.Once
	redisCtx    = context.Background()
)

func getRedisClient() *redis.Client {
	redisOnce.Do(func() {
		config := conf.GetConfig()
		redisClient = redis.NewClient(&redis.Options{
			Addr:     config.Redis.Host + ":" + config.Redis.Port,
			Password: config.Redis.Password,
			DB:       0,
		})
	})

	return redisClient
}
