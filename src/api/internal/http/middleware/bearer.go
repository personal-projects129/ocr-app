package middleware

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"ocrapp/src/api/internal/conf"
	"ocrapp/src/api/internal/http/util"
	"ocrapp/src/api/internal/model"
	"strings"

	"github.com/dgrijalva/jwt-go"
)

// ResponseWriterWithVerifiedBearer http.ResponseWriter with verified bearer token
type ResponseWriterWithVerifiedBearer struct {
	writer http.ResponseWriter
	user   model.User
}

type validateBearerResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// ValidateBearer validates bearer token
func ValidateBearer(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	// tokenRepository := repository.NewSessionRedisRepository()
	authHeader := req.Header.Get("Authorization")

	if !strings.HasPrefix(authHeader, "Bearer ") {
		response := validateBearerResponse{
			Code:    401,
			Message: "Missing Authorization: Bearer",
		}
		util.JSONResponse(response, response.Code, res)
		return
	}

	token, err := jwt.Parse(strings.Split(authHeader, " ")[1], func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("Unexpected Signing method")
		}

		return []byte(conf.GetConfig().HMACSecret), nil
	})
	if err != nil || !token.Valid {
		log.Println(err)
		response := validateBearerResponse{
			Code:    403,
			Message: "Invalid Bearer Token",
		}
		util.JSONResponse(response, response.Code, res)
		return
	}

	userMap, ok := token.Claims.(jwt.MapClaims)["user"]
	data, err := json.Marshal(userMap)
	log.Println(string(data))
	if err != nil || !ok {
		log.Println(err)
		response := validateBearerResponse{
			Code:    403,
			Message: "Invalid Payload",
		}
		util.JSONResponse(response, response.Code, res)
		return
	}

	user := model.User{}
	err = json.Unmarshal(data, &user)

	if err != nil {
		log.Println(err)
		response := validateBearerResponse{
			Code:    403,
			Message: "Invalid Bearer Token",
		}
		util.JSONResponse(response, response.Code, res)
		return
	}

	// tokenSlice := strings.Split(token, ":")
	// if len(tokenSlice) != 2 {
	// 	response := validateBearerResponse{
	// 		Code:    403,
	// 		Message: "Invalid Bearer",
	// 	}
	// 	util.JSONResponse(response, response.Code, res)
	// 	return
	// }

	// bearer, err := tokenRepository.GetBearer(tokenSlice[0])
	// if err != nil || bearer.Bearer != token {
	// 	response := validateBearerResponse{
	// 		Code:    403,
	// 		Message: "Invalid Bearer: " + token,
	// 	}
	// 	util.JSONResponse(response, response.Code, res)
	// 	return
	// }

	next(&ResponseWriterWithVerifiedBearer{
		writer: res,
		user:   user,
	}, req)

}

// Header writes Header

// GetUserInfo returns information about authenticated user
func (res *ResponseWriterWithVerifiedBearer) GetUserInfo() model.User {
	return res.user
}

// Header calls wrapped implementation
func (res *ResponseWriterWithVerifiedBearer) Header() http.Header {
	return res.GetWriter().Header()
}

// Write calls wrapped implementation
func (res *ResponseWriterWithVerifiedBearer) Write(data []byte) (int, error) {
	return res.GetWriter().Write(data)
}

// WriteHeader calls wrapped implementation
func (res *ResponseWriterWithVerifiedBearer) WriteHeader(statusCode int) {
	res.GetWriter().WriteHeader(statusCode)
}

// GetWriter returns wrapped response writer
func (res *ResponseWriterWithVerifiedBearer) GetWriter() http.ResponseWriter {
	return res.writer
}

// ResponseWriterWithVerifiedBearerOf return ResponseWriter with verified bearer
func ResponseWriterWithVerifiedBearerOf(res http.ResponseWriter) *ResponseWriterWithVerifiedBearer {
	wrappedRes := getWrappedResponseWriter(res)

	withVerifiedBearer, isWithVerifiedBearer := wrappedRes.(*ResponseWriterWithVerifiedBearer)
	if !isWithVerifiedBearer {
		return ResponseWriterWithVerifiedBearerOf(wrappedRes.GetWriter())
	}
	return withVerifiedBearer

}
