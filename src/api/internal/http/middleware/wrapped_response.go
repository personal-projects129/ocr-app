package middleware

import (
	"log"
	"net/http"
)

// WrappedResponseWriter wraps default http.ResponseWriter
type WrappedResponseWriter interface {
	GetWriter() http.ResponseWriter
}

func getWrappedResponseWriter(res http.ResponseWriter) WrappedResponseWriter {
	wrappedRes, isWrapped := res.(WrappedResponseWriter)
	if !isWrapped {
		log.Fatalf("%T not found, please check code base", "ResponseWriterWithVerifiedBearer")
	}
	return wrappedRes
}
