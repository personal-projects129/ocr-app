package middleware

import (
	"log"
	"net/http"
)

// One dummy
func One(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	log.Println("one before next")
	next(res, req)
	log.Println("one after next")
}

// Two dummy
func Two(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	log.Println("two before next")
	next(res, req)
	log.Println("two after next")
}

// Three dummy
func Three(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	log.Println("three before next")
	next(res, req)
	log.Println("three after next")
}

// ResponseWriterWithAnotherData test
type ResponseWriterWithAnotherData struct {
	data   string
	writer http.ResponseWriter
}

// AnotherData test
func AnotherData(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	next(&ResponseWriterWithAnotherData{
		data:   "test data",
		writer: res,
	}, req)
}

// Header writes Header
func (res *ResponseWriterWithAnotherData) Header() http.Header {
	return res.writer.Header()
}

// Write write data
func (res *ResponseWriterWithAnotherData) Write(data []byte) (int, error) {
	return res.writer.Write(data)
}

// WriteHeader writes status code
func (res *ResponseWriterWithAnotherData) WriteHeader(statusCode int) {
	res.writer.WriteHeader(statusCode)
}

// GetData returns information about authenticated user
func (res *ResponseWriterWithAnotherData) GetData() string {
	return res.data
}

// GetWriter returns wrapped response writer
func (res *ResponseWriterWithAnotherData) GetWriter() http.ResponseWriter {
	return res.writer
}

// ResponseWriterWithAnotherDataOf return ResponseWriter with verified bearer
func ResponseWriterWithAnotherDataOf(res http.ResponseWriter) *ResponseWriterWithAnotherData {
	wrappedRes := getWrappedResponseWriter(res)

	withAnotherData, isWithAnotherData := wrappedRes.(*ResponseWriterWithAnotherData)
	if !isWithAnotherData {
		return ResponseWriterWithAnotherDataOf(wrappedRes.GetWriter())
	}
	return withAnotherData
}
