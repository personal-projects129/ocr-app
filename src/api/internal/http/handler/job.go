package handler

import (
	"net/http"
	"ocrapp/src/api/internal/http/middleware"
	"ocrapp/src/api/internal/http/util"
	"ocrapp/src/api/internal/publisher"
	"ocrapp/src/api/internal/repository"
	"ocrapp/src/api/internal/service"
	"strconv"

	"github.com/gorilla/mux"
)

// GetJob returns job with id
func GetJob(res http.ResponseWriter, req *http.Request) {
	jobID, _ := strconv.ParseInt(mux.Vars(req)["id"], 10, 64)
	userID := middleware.ResponseWriterWithVerifiedBearerOf(res).GetUserInfo().GoogleID

	svc := service.NewJobService().
		WithJobRepository(repository.NewJobMariaDBRepository())

	result, err := svc.GetJob(userID, jobID)

	if handleError(err, res, req) {
		return
	}

	util.JSONResponse(result, 200, res)
}

// GetJobs returns jobs
// query parameters
// id -- starting jobID
// n -- total jobs to fetch
func GetJobs(res http.ResponseWriter, req *http.Request) {
	userID := middleware.ResponseWriterWithVerifiedBearerOf(res).GetUserInfo().GoogleID
	values := req.URL.Query()
	id, err := strconv.ParseInt(values.Get("id"), 10, 64)
	if err != nil {
		id = 0
	}
	n, err := strconv.ParseInt(values.Get("n"), 10, 64)
	if err != nil {
		n = 10
	}
	svc := service.NewJobService().
		WithJobRepository(repository.NewJobMariaDBRepository())
	result, svcErr := svc.GetJobs(userID, id, n)

	if handleError(svcErr, res, req) {
		return
	}

	util.JSONResponse(result, 200, res)
}

// DeleteJob deletes a job
func DeleteJob(res http.ResponseWriter, req *http.Request) {
	id, _ := strconv.ParseInt(mux.Vars(req)["id"], 10, 64)
	userID := middleware.ResponseWriterWithVerifiedBearerOf(res).GetUserInfo().GoogleID

	result, svcErr := service.NewJobService().
		WithEventPublisher(publisher.NewEventNSQPublisher()).
		WithJobRepository(repository.NewJobMariaDBRepository()).DeleteJob(userID, id)

	if handleError(svcErr, res, req) {
		return
	}

	util.JSONResponse(result, 200, res)
}
