package handler

import (
	"log"
	"net/http"
	"ocrapp/src/api/internal/errors"
	"ocrapp/src/api/internal/http/util"
	"strconv"
)

func handleError(err errors.ServiceError, res http.ResponseWriter, req *http.Request) bool {
	if err.Code < 400 {
		return false
	}
	if err.Code >= 500 {
		res.WriteHeader(err.Code)
		res.Write([]byte("Server error: " + strconv.Itoa(err.Code)))
		log.Printf("Server error:\n%s\n", err.Error())
		return true
	}
	util.JSONResponse(err, err.Code, res)
	return true
}
