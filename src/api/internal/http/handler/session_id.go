package handler

import "net/http"

func setSessionIDCookie(sessionID string, res http.ResponseWriter) {
	http.SetCookie(res, &http.Cookie{
		Name:     "session_id",
		Value:    sessionID,
		HttpOnly: true,
		SameSite: http.SameSiteLaxMode,
		Secure:   true,
		Path:     "/",
	})
}
