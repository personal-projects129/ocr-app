package util

import (
	"encoding/json"
	"log"
	"net/http"
)

// JSONResponse returns json reponse to client
func JSONResponse(body interface{}, code int, res http.ResponseWriter) {
	data, err := json.Marshal(body)
	if err != nil {
		res.WriteHeader(500)
		res.Write([]byte("Server error"))
		log.Println("Failed to parse json response body")
		return
	}

	res.Header().Set("content-type", "application/json")
	res.WriteHeader(code)
	res.Write(data)
}
