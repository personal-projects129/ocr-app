package route

import (
	"net/http"
	"ocrapp/src/api/internal/http/handler"
	"ocrapp/src/api/internal/http/middleware"

	"github.com/gorilla/mux"
)

func attachJobRoutes(r *mux.Router) {
	r.HandleFunc("", middleware.
		WithMiddleware(handler.GetJobs,
			middleware.ValidateBearer)).Methods(http.MethodGet)

	r.HandleFunc("/{id:[0-9]+}",
		middleware.
			WithMiddleware(handler.GetJob, middleware.
				ValidateBearer)).Methods(http.MethodGet)

	r.HandleFunc("/{id:[0-9]+}", middleware.
		WithMiddleware(handler.DeleteJob, middleware.
			ValidateBearer)).Methods(http.MethodDelete)
}
