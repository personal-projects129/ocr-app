package route

import (
	"net/http"
	"ocrapp/src/api/internal/http/handler"
	"ocrapp/src/api/internal/http/middleware"

	"github.com/gorilla/mux"
)

func attachUploadRoutes(r *mux.Router) {
	r.HandleFunc("", middleware.
		WithMiddleware(handler.UploadFile,
			middleware.ValidateBearer)).
		Methods(http.MethodPost)
}
