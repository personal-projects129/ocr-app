package service

import (
	"log"
	"net/url"
	"time"

	"ocrapp/src/api/internal/errors"
	"ocrapp/src/api/internal/model"
	"ocrapp/src/api/internal/repository"
	"ocrapp/src/api/pkg/random"
)

const googleAuthBaseEndpoint = "https://accounts.google.com/o/oauth2/v2/auth"
const googleTokenEndpoint = "https://oauth2.googleapis.com/token"

// GoogleOAuthService OAuth service struct
type GoogleOAuthService struct {
	stateRepo   repository.StateRepository
	tokenRepo   repository.TokenRepository
	sessionRepo repository.SessionRepository
}

// NewGoogleOAuthService returns new OAuth service
func NewGoogleOAuthService() GoogleOAuthService {
	return GoogleOAuthService{
		stateRepo: nil,
		tokenRepo: nil,
	}
}

// WithStateRepository sets the state repository
func (service GoogleOAuthService) WithStateRepository(stateRepo repository.StateRepository) GoogleOAuthService {
	service.stateRepo = stateRepo
	return service
}

// WithTokenRepository sets the token repository
func (service GoogleOAuthService) WithTokenRepository(tokenRepo repository.TokenRepository) GoogleOAuthService {
	service.tokenRepo = tokenRepo
	return service
}

// WithSessionRepository sets the session repository
func (service GoogleOAuthService) WithSessionRepository(sessionRepo repository.SessionRepository) GoogleOAuthService {
	service.sessionRepo = sessionRepo
	return service
}

// GetRedirectURI returns redirect uri
func (service GoogleOAuthService) GetRedirectURI(state map[string]string) (string, errors.ServiceError) {
	err := service.stateRepo.StoreState(state["state"], state, 5*time.Minute)
	if err != nil {
		return "", errors.ServiceError{
			Code:    500,
			Message: err.Error(),
		}
	}

	return service.getRedirectURI(state), errors.ServiceError{}
}

// GetToken returns token from google api
func (service GoogleOAuthService) GetToken(stateKey string, code string, clientID string, clientSecret string, redirectURI string) (string, errors.ServiceError) {

	state, err := service.stateRepo.GetState(stateKey)

	if state["state"] != stateKey {
		return "", errors.ServiceError{
			Code:    403,
			Message: "Invalid State",
		}
	}

	if service.stateRepo == nil {
		log.Fatal("GoogleOAuthService.stateRepo is nil")
	}

	service.stateRepo.DeleteState(stateKey)

	if service.tokenRepo == nil {
		log.Fatal("GoogleOAuthService.tokenRepo is nil")
	}
	params := make(map[string]string)
	params["code"] = code
	params["client_id"] = clientID
	params["client_secret"] = clientSecret
	params["redirect_uri"] = state["redirect_uri"]
	params["grant_type"] = "authorization_code"

	result, err := service.tokenRepo.GetToken(params)

	if err != nil {
		return "", errors.ServiceError{
			Code:    500,
			Message: err.Error(),
		}
	}

	return service.login(result)
}

func (service GoogleOAuthService) login(token string) (string, errors.ServiceError) {

	googleToken := model.ParseGoogleTokenFromString(token)
	// mostlikely verification of jwt token is not needed
	// i only need openid
	/*
		jwtToken, _ := jwt.Parse(googleToken.IDToken, func(token *jwt.Token) (interface{}, error) {
			// Todo validate token
			return nil, nil
		})
	*/
	jwtPayload, err := model.ParseJWTPayloadFromJWTString(googleToken.IDToken)

	if err != nil {
		return "", errors.ServiceError{
			Code:    500,
			Message: err.Error(),
		}
	}

	sessionID := jwtPayload.Subject + ":" + random.GenerateBase64String(64)

	service.sessionRepo.StoreSession(jwtPayload.Subject, model.Session{
		Token: googleToken,
		ID:    sessionID,
		User: model.User{
			ID:         0,
			GoogleID:   jwtPayload.Subject,
			Email:      jwtPayload.Email,
			GivenName:  jwtPayload.GivenName,
			FamilyName: jwtPayload.FamilyName,
			Picture:    jwtPayload.Picture,
		},
	})
	return sessionID, errors.ServiceError{}
}

func (service GoogleOAuthService) getRedirectURI(state map[string]string) string {
	values := make(url.Values)
	for key, value := range state {
		values.Set(key, value)
	}
	return googleAuthBaseEndpoint + "?" + values.Encode()
}
