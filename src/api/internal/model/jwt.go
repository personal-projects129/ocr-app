package model

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"strings"
)

// GoogleJWTPayload payload section of the idtoken returned by google openid endpoint
type GoogleJWTPayload struct {
	Subject    string `json:"sub"`
	Email      string `json:"email"`
	Picture    string `json:"picture"`
	GivenName  string `json:"given_name"`
	FamilyName string `json:"family_name"`
}

// ParseJWTPayloadFromJWTString returns GoogleJWTPayload
func ParseJWTPayloadFromJWTString(jwt string) (GoogleJWTPayload, error) {

	payload := GoogleJWTPayload{}
	splitted := strings.Split(jwt, ".")
	if len(splitted) != 3 {
		return payload, errors.New("Invalid jwt string: " + jwt)
	}
	decodedPayload := []byte{}
	decodedPayload, err := base64.RawURLEncoding.DecodeString(splitted[1])
	if err != nil {
		return payload, err
	}

	err = json.Unmarshal(decodedPayload, &payload)

	if err != nil {
		return payload, err
	}

	return payload, nil
}
