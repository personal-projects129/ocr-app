package model

import (
	"encoding/json"
	"log"
)

// GoogleToken token returned by google openid endpoint
type GoogleToken struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   uint64 `json:"expires_in"`
	Scope       string `json:"scope"`
	TokenType   string `json:"token_type"`
	IDToken     string `json:"id_token"`
}

// ParseGoogleTokenFromString returns GoogleToken struct parsed from string
func ParseGoogleTokenFromString(token string) GoogleToken {
	googleToken := GoogleToken{}
	err := json.Unmarshal([]byte(token), &googleToken)
	if err != nil {
		log.Println(err)
	}
	return googleToken
}
