package model

// Session data to store
type Session struct {
	ID    string      `json:"id"`
	Token GoogleToken `json:"token"`
	User  User        `json:"user"`
}
