package model

// BearerToken schema
type BearerToken struct {
	User   User   `json:"user"`
	Bearer string `json:"bearer"`
}
