package main

import (
	"net/http"
	"ocrapp/src/event-hub/internal/conf"
	"ocrapp/src/event-hub/internal/http/handler"
	"os"
)

func main() {
	config := conf.LoadConfig(os.Args[1])
	http.ListenAndServe(config.ListenAddress, handler.NewUpgradeHandler())
}
