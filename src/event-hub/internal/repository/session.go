package repository

import (
	"ocrapp/src/event-hub/internal/model"
)

// SessionRepository interface
type SessionRepository interface {
	GetSession(key string) (model.Session, error)
}
