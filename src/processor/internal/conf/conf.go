package conf

import (
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v2"
)

// AppConfig app AppConfiguration struct
type AppConfig struct {
	NSQ struct {
		NSQDHost        string   `yaml:"nsqdHost"`
		NSQLookupDHosts []string `yaml:"nsqlookupdHosts"`
		MaxInFlight     int      `yaml:"maxInFlight"`
	} `yaml:"nsq"`
	MariaDB struct {
		Host     string `yaml:"host"`
		Port     uint16 `yaml:"port"`
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		Database string `yaml:"database"`
	} `yaml:"mariadb"`
	ImageProvider struct {
		Host string `yaml:"host"`
	} `yaml:"imageProvider"`
}

var (
	instance *AppConfig
)

// LoadConfig - Load Api AppConfiguration
func LoadConfig(path string) *AppConfig {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Printf("Failed to load AppConfiguration file: %s\n", path)
		log.Fatal(err)
	}

	AppConfig := AppConfig{}
	err = yaml.Unmarshal([]byte(data), &AppConfig)

	if err != nil {
		log.Printf("Failed to Serialize AppConfiguration file should be a valid yaml: \n%s", string(data))
		log.Fatal(err)
	}

	log.Printf("Loaded AppConfiguration file: %s\n%s\n", path, string(data))
	instance = &AppConfig

	return instance
}

// GetConfig - Return Loaded AppConfiguration
func GetConfig() *AppConfig {
	return instance
}
