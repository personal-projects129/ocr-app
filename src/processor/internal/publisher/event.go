package publisher

import "ocrapp/src/processor/internal/model"

// EventPublisher publish events
type EventPublisher interface {
	PublishJobEvent(userID string, event model.JobEvent) error
}
