package repository

import "ocrapp/src/processor/internal/model"

// JobRepository job repository
type JobRepository interface {
	UpdateJob(model.Job) (model.Job, error)
}
