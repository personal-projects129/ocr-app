package subscriber

import (
	"log"
	"ocrapp/src/processor/internal/conf"

	"github.com/nsqio/go-nsq"
)

func newNSQConsumer(topic string, channel string) *nsq.Consumer {
	config := conf.GetConfig()
	nsqConfig := nsq.NewConfig()
	nsqConfig.MaxInFlight = config.NSQ.MaxInFlight

	subscriber, err := nsq.NewConsumer(topic, channel, nsqConfig)

	if err != nil {
		log.Fatalf("Failed to create nsq consumer:\n%s\n", err)
	}
	return subscriber
}
