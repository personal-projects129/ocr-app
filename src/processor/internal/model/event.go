package model

// JobEvent events base
type JobEvent struct {
	Type      string `json:"type"`
	TimeStamp string `json:"timestamp"`
	Job       Job    `json:"job"`
}

const (
	// JobEventTypeFailed job failed event
	JobEventTypeFailed = "failed"
	// JobEventTypeDispatched job dispatched
	JobEventTypeDispatched = "dispatched"
	// JobEventTypeInProgress job inprogress
	JobEventTypeInProgress = "inprogress"
	// JobEventTypeSuccess job success
	JobEventTypeSuccess = "success"
	// JobEventTypeDeleted job deleted
	JobEventTypeDeleted = "deleted"
	// JobEventTypeUpdated job updated
	JobEventTypeUpdated = "updated"
)
