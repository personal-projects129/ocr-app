package main

import (
	"log"
	"net/http"
	"ocrapp/src/processor/internal/conf"
	"ocrapp/src/processor/internal/subscriber"
	"os"
)

func main() {
	conf.LoadConfig(os.Args[1])
	jobConsumer := subscriber.NewJobNSQConsumer()
	err := jobConsumer.Start()
	if err != nil {
		log.Fatalf("Error while trying to start JobNSQConsumer: \n%s\n", err)
	}

	http.ListenAndServe(":8080", nil)
}
