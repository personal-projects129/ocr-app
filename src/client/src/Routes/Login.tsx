import { Component } from "react";
import '../css/Routes/Login.css';
import Banner from "../Components/Login/Banner";
import SignInButton from "../Components/Login/SignInButton";
import Footer from "../Components/Login/Footer";


export default class Login extends Component {
    render() {
        return (
            <div className="main-container">
                <div className="banner-container">
                    <Banner/>
                </div>
                <div className="sign-in-button-container">
                    <SignInButton/>
                </div>
                <div className="footer-container">
                    <Footer/>
                </div>
            </div>
        )
    }
}