import { Component } from 'react'
import User from '../Api/Schemas/User'
import '../css/Routes/Home.css'
import NavigationBar from '../Components/Navigation/NavigationBar'
import FloatingUploadButton from '../Components/FloatingUploadButton'
import UploadProgressContainer from '../Components/Home/UploadProgressContainer'
import JobsContainer from '../Components/Home/JobsContainer'
import EventHub from '../Hub/EventHub'
import JobStore from '../Stores/JobStore'
import ApiClient from '../Api/ApiClient'

interface HomeState {
    User: User
}


export default class Home extends Component<any, HomeState> {
    constructor(props: any) {
        super(props)
    }
    render() {
        return (
            <div className="home-container">
                <NavigationBar/>
                <FloatingUploadButton/>
                <UploadProgressContainer/>
                <JobsContainer/>
            </div>
        )
    }
    componentDidMount() {
        window.onmousewheel = this.onscrollEvent.bind(this)
        window.ontouchmove = () => { window.onmousewheel?.call(window, {} as any) }
        EventHub.GetInstance().startListening()
    }
    componentWillUnmount() {
        EventHub.GetInstance().close()
    }

    private async onscrollEvent () {
        if (window.innerHeight + window.scrollY >= window.document.body.offsetHeight) {
            window.onmousewheel = () => {}
            let store = JobStore.getStore()
            let jobs = await new ApiClient().GetJobs(store.getState().oldestID, 10)
            jobs.forEach(job => {
                store.dispatch({
                    type: "set",
                    job: job
                })
            })
            setTimeout(() => {
                window.onmousewheel = this.onscrollEvent.bind(this)
            }, 500)
        }
    }
}
