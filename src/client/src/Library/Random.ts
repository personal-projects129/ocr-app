import { randomBytes } from "crypto"



export function generateRandomString(size: number): string {
    let bytes = randomBytes(size)
    let string = ''
    bytes.forEach((byte) => {
        string += String.fromCharCode(byte)
    })
    return btoa(string)
}