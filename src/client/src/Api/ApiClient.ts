import { IJob } from "../Stores/JobStore";
import Auth from "./Auth/Auth";
import IJobSchema from "./Schemas/Job";

// ApiClient 
export default class ApiClient {

    private async executeWithRetry(fnRequest: () => Promise<Response>): Promise<Response> {
        let response = await fnRequest()
        if (response.status === 403) {
            await Auth.GetInstance().Authenticate()
        } else {
            return response
        }
        return await fnRequest()
    }

    /**
     * 
     * @param from 
     * @param count 
     */
    public async GetJobs(from: number, count: number): Promise<IJob[]> {
        let response = await this.executeWithRetry(() => {
            return fetch(`/api/jobs?id=${from}&n=${count}`, {
                headers: {
                    Authorization: `Bearer ${Auth.GetInstance().GetBearer().Bearer}`
                }
            })
        })
        if (response.status > 399) {
            return []
        }
        let jobs = await response.json() as IJobSchema[]
        let jobsOut = new Array<IJob>()
        jobs.forEach(job => {
            jobsOut.push({
                completed: job.completed,
                dispatched: job.dispatched,
                id: job.id,
                imageID: job.image_id,
                name: job.name,
                result: job.result,
                started: job.started,
                status: job.status,
                timestamp: Number.MAX_VALUE,
                userID: job.user_id,
            })
        })
        return jobsOut
    }

    public async DeleteJob(id: number): Promise<IJob> {
        let response = await this.executeWithRetry(() => {
            return fetch(`/api/jobs/${id}`, {
                headers: {
                    Authorization: `Bearer ${Auth.GetInstance().GetBearer().Bearer}`
                },
                method: "DELETE"
            })
        })
        if (response.status > 399) {
            return {} as IJob
        }
        return await response.json() as IJob
    }

}