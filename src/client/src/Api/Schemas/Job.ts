

export default interface IJobSchema {
    id: number
    user_id: string
    name: string
    image_id: string
    status: string
    dispatched: string
    started: string
    completed: string
    result: string
}