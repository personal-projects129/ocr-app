
/**
 * User Schema
 * 
 * @author zildjia
 * @since 2021.01.01
 */
export default interface User {
    ID: number
    GoogleID: string
    Email: string
    GivenName: string
    FamilyName: string
    Picture: string
}