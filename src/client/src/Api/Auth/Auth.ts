import Bearer from "../Schemas/Bearer";

export interface AuthenticateResult {
    Success: boolean
    Bearer: Bearer | null
}

export default class Auth {

    private bearer!: Bearer
    private static instance: Auth


    private constructor() { }

    public static GetInstance(): Auth {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new Auth()
        }
        return this.instance
    }

    public GetBearer(): Bearer {
        return this.bearer
    }

    public Authenticate(): Promise<AuthenticateResult> {
        return new Promise(async (fResolve) => {
            let response = await fetch("/api/auth/web/bearer")
            if (response.status >= 400) {
                fResolve({
                    Success: false,
                    Bearer: null
                })
                window.location.href= "/login"
                return
            }
            let body = await response.json()
            this.bearer = {
                Bearer: body.bearer as string,
                User: {
                    Email: body.user.email,
                    FamilyName: body.user.family_name,
                    GivenName: body.user.given_name,
                    GoogleID: body.user.google_id,
                    ID: body.user.id,
                    Picture: body.user.picture
                }
            }
            fResolve({
                    Success: true,
                    Bearer: this.bearer
            })
        })
    }
}