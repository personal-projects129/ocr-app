import Auth from '../Auth/Auth';
import Upload, {UploadError} from './Upload'

/**
 * Uploader
 * use this to upload files
 * 
 * @author zildjian
 * @since 2021.01.03.
 */
export default class Uploader {

    private constructor() {
        throw Error("Usage of new Uploader() is not allowed")
    }
    public static UploadFile(file: File): Upload {
        let upload = new FileUpload('/api/upload', file);
        return upload
    }
}

/**
 * FileUpload
 * Actual upload logic
 * 
 * @author zildjian
 * @since 2021.01.03.
 */
export class FileUpload extends Upload {
    private path!: string
    private request!: XMLHttpRequest

    constructor(path: string, file: File) {
        super()
        this.file = file
        this.path = path
    }

    public start(): Upload {
        this.request = new XMLHttpRequest()
        this.request.open('POST', this.path, true)
        this.bindEventsToRequest()
        this.setHeaders()

        let formData = this.createFormData()
        this.request.send(formData)
        return this
    }

    public getFile() : File {
        return this.file 
    }

    private bindEventsToRequest(): void {
        this.request.upload.onprogress = (event) => {
            let percentage = Math.ceil(event.loaded / this.file.size * 100)
            
            this.fnProgress(percentage > 100 ? 100 : percentage, this.file)
        }
    
        this.request.onload = () => {
            if (this.request.status < 400) {
                this.fnDone()
            } else {
                this.fnError({
                    code: this.request.status,
                    error: new Error(`Failed to upload: ${this.request.responseText}`)
                } as UploadError)
            }
        }
    }

    private setHeaders(): void {
        this.request.setRequestHeader("Authorization", `Bearer ${Auth.GetInstance().GetBearer().Bearer}`)
    }

    private createFormData(): FormData {
        let formData = new FormData();
        formData.set('file', this.file, this.file.name)
        return formData
    }


}