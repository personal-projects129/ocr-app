import UploadStore from '../../Stores/UploadStore'
import Auth from '../Auth/Auth'
import Upload from './Upload'
import Uploader from './Uploader'

/**
 * UploadBatch
 * Performs batched uploading
 * 
 * @author zildjian
 * @since 2020.01.16.
 */
export default class UploadBatch {

    private uploads: Upload[]
    private uploadsRetry: Upload[]
    private doneResult: any[]
    private errorResult: any[]
    private fnDone: (result: any[]) => void
    private fnError: (result: any[]) => void

    constructor() {
        this.uploads = new Array<Upload>()
        this.uploadsRetry = new Array<Upload>()
        this.doneResult = new Array<any>()
        this.errorResult = new Array<any>()
        this.fnDone = () => {}
        this.fnError = () => {}
    }


    public Add(upload: Upload) : UploadBatch {
        let store = UploadStore.GetStore()
        

        upload.error(reason => {
            if (reason.code === 403) {
                this.uploadsRetry.push(upload)
            } else {
                this.errorResult.push(reason)
                store.dispatch({
                    type: "update",
                    upload: {
                        fileName: upload.getFile().name,
                        progress: 100,
                        status: "failed",
                        id: upload.getID()
                    }
                })
            }
            this.executeEventHooks()
        }).done(() => {
            store.dispatch({
                type: "clear",
                upload: {
                    fileName: upload.getFile().name,
                    progress: 0,
                    status: 'pending',
                    id: upload.getID(),
                }
            })
        }).progress((progress, file) => {
            store.dispatch({
                type: "update",
                upload: {
                    fileName: file.name,
                    progress: progress,
                    status: 'pending',
                    id: upload.getID(),
                }
            })
        })

        store.dispatch({
            type: "push",
            upload: {
                fileName: upload.getFile().name,
                progress: 0,
                status: 'pending',
                id: upload.getID(),
            }
        })
        this.uploads.push(upload)
        return this
    }

    public Start() : UploadBatch {
        Auth.GetInstance().Authenticate().then(() => {
            for(let index=0; index < this.uploads.length; index++) {
                this.uploads[index].start()
                this.executeEventHooks()
            }
        })
        return this
    }

    public done(fnDone: (result: any[]) => void) : UploadBatch {
        this.fnDone = fnDone
        return this
    }

    public error(fnError: (result: any[]) => void) : UploadBatch{
        this.fnError = fnError
        return this
    }

    private executeEventHooks() {
        if (this.errorResult.length + this.doneResult.length + this.uploadsRetry.length === this.uploads.length) this.retry()
        if (this.errorResult.length + this.doneResult.length === this.uploads.length) this.complete()
    }

    private retry() {
        Auth.GetInstance().Authenticate().then(() => {
            this.uploadsRetry.forEach((current) => {
                current.start()
            })
        })
    }

    private complete() {
        if (this.doneResult.length > 0) this.fnDone(this.doneResult)
        if (this.errorResult.length > 0) this.fnError(this.errorResult)
    }
}