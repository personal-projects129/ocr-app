import { generateRandomString } from "../../Library/Random"


/**
 * Upload
 * File to be uploaded
 * 
 * @author zildjian
 * @since 2021.01.03.
 */
export default abstract class Upload {
    protected file!: File
    protected fnProgress!: (progress: number, file: File) => void
    protected fnDone!: () => void
    protected fnError!: (reason: UploadError) => void
    protected id: string
    public constructor() {
        this.fnProgress = () => {}
        this.fnDone = () => {}
        this.fnError = () => {
            throw new Error("Unhandled Upload Error")
        }
        this.id = generateRandomString(64)
    }
    
    public progress(fnProgress: (progress: number, file: File) => void): Upload {
        this.fnProgress = fnProgress
        return this
    }

    public done(fnDone: () => void): Upload {
        this.fnDone = fnDone
        return this;
    }
    public error(fnError: (reason: UploadError) => void) {
        this.fnError = fnError
        return this;
    }

    public abstract start(): Upload

    public getFile(): File {
        return this.file
    }

    public getID(): string{
        return this.id
    }
}

/**
 * 
 */
export interface UploadError {
    code: number
    error: Error
}