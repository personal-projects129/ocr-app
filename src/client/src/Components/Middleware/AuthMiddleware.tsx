import { Component } from "react"
import { Redirect } from "react-router-dom";
import Auth from "../../Api/Auth/Auth";

interface AuthMiddlewareState {
    authenticateState: "pending" | boolean
}

interface AuthMiddlewareProps {
    children?: any;
}

export default class AuthMiddleware extends Component<AuthMiddlewareProps, AuthMiddlewareState> {

    constructor(props: AuthMiddlewareProps) {
        super(props)
        
        if (Auth.GetInstance().GetBearer() === undefined) {
            this.state = {
                authenticateState: "pending"
            }
        } else {
            this.state = {
                authenticateState: true
            }
        }
    }

    render() {
        if (this.state.authenticateState === "pending") {
            return (
                <div>
                    <h1>Loading . . .</h1>
                </div>
            )
        }
        if (this.state.authenticateState === false)
            return (
                <Redirect to="/login" />
            );

        return (
            this.props.children
        );
    }

    async componentDidMount() {
        if (this.state.authenticateState !== true) {    
            let result = await Auth.GetInstance().Authenticate()
            this.setState({ authenticateState: result.Success as boolean })
        }

    }
}