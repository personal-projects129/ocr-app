import { Component } from 'react';
import '../css/Components/FloatingActionButton.css'
import uploadIcon from '../assets/icons/upload-icon.png';
import Uploader from '../Api/Upload/Uploader';
import UploadBatch from '../Api/Upload/UploadBatch';



export default class FloatingUploadButton extends Component {
    render() {
        return (
            <div className="floating-upload-button" onClick={this.selectFiles}>
                <input id="file-selector" type="file" hidden={true} multiple={true} accept="image/*" />
                <img src={uploadIcon} alt="uploadIcon"/>
            </div>
        );
    }

    componentDidMount() {
        this.bindEventHandlers()
    }

    private bindEventHandlers() {
        let inputFile = document.getElementById("file-selector")!
        inputFile.onchange = this.inputFileOnChange.bind(this)
    }

    private selectFiles() {
        document.getElementById("file-selector")?.click()
    }

    private inputFileOnChange(event: Event) {
        event.preventDefault()
        let target = event.target as HTMLInputElement
        let files = target.files!
        let batch = new UploadBatch()
        
        for (let ctr = 0; ctr< files.length; ctr ++) {
            batch.Add(Uploader.UploadFile(files[ctr]))
        }
        batch.Start()
        target.value = ''
        return
    }
}