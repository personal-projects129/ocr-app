import { Component, ReactElement } from "react"
import Job, {IJobProps} from "./Job"
import "../../css/Components/Home/JobsContainer.css"
import JobStore, { IJobStoreState } from "../../Stores/JobStore"
import ApiClient from "../../Api/ApiClient"

export default class JobsContainer extends Component<any, IJobStoreState> {

    constructor(props: any) {
        super(props)
        this.state = {
            oldestID: JobStore.getStore().getState().oldestID,
            jobs: new Map(JobStore.getStore().getState().jobs)
        }
    }
    render() {
        let jobs = new Array<ReactElement<IJobProps>>()
        this.state.jobs.forEach(job => {
            jobs.unshift(<Job id={job.id} key={job.id} image={`/image/${job.imageID}`} name={job.name}/>)
        })

        jobs.sort((a, b) => {
            if (a.props.id > b.props.id) {
                return -1
            }
            return 1
        })
        return (
            <div className="job-container">
                {jobs}
            </div>
        )
    }

    componentDidMount() {
        let store = JobStore.getStore()
        store.subscribe(() => {
            if (this.state === null || this.state === undefined || this.state.jobs.size !== store.getState().jobs.size) {
                this.setState({
                    jobs: new Map(store.getState().jobs)
                })
            }
        })
        new ApiClient().GetJobs(store.getState().oldestID, 10).then(jobs => {
            jobs.forEach(job => {
                job.timestamp = Number.MAX_VALUE
                store.dispatch({
                    type: "set",
                    job: job,
                })
            })
        })
    }
}