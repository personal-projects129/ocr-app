import { Component } from "react";
import Sample from "../../assets/img/1.png"
import Clipboard from "../../assets/icons/clipboard.png"
import Inspect from "../../assets/icons/inspect.png"
import Trash from "../../assets/icons/trash.png"
import Fold from "../../assets/icons/fold.png"
import Loading from "../../assets/icons/loading.png"
import "../../css/Components/Home/Job.css"
import JobStore, { IJob } from "../../Stores/JobStore";
import { Unsubscribe } from "redux";
import ApiClient from "../../Api/ApiClient";

export interface IJobProps {
    name: string
    image: string
    id: number
}

export default class Job extends Component<IJobProps, IJob> {
    private unsub!: Unsubscribe

    constructor(props: IJobProps) {
        super(props)
        this.state = JobStore.getStore().getState().jobs.get(props.id)!
    }

    render() {
        return (
            <div className="job">
                <div className="title-bar">
                    <span className="name">{this.props.name}</span>
                    <span className={`fold-icon ${this.state.status === "success" ? "" : "hidden"}`}><img src={Fold} onClick={this.toggleFold} className=""/></span>
                    <span className={`loading-icon ${this.state.status === "success" ? "hidden" : ""} `}><img className={`${this.state.status === "inprogress" ? "rotating": ""}
                    `}src={Loading}/></span>
                </div>
                <div className="body hidden">
                    <div className="image-container">
                        <img src={this.props.image}/>
                    </div>
                    <div className="result-container">
                        {this.state.result}
                    </div>
                    <div className="footer">
                        <div className="delete" onClick={this.delete.bind(this)}><img src={Trash}/></div>
                        {/*<div className="inspect"><img src={Inspect}/></div>*/}
                        <div className="clipboard" onClick={this.copyToClipBoard.bind(this)}><img src={Clipboard}/></div>
                        <div className={`status ${this.state.status==="pending" ? "status-pending": ""} ${this.state.status==="inprogress" ? "status-processing": ""} ${this.state.status==="success" ? "status-done": ""}`}>{this.state.status.toLocaleUpperCase()}</div>
                    </div>
                </div>
            </div>
        )
    }
    componentDidMount() {
        let store = JobStore.getStore()
        
        this.unsub = store.subscribe(() => {
            let currentState = store.getState().jobs.get(this.props.id)
            if (currentState == undefined) {
                this.unsub()
                return
            }

            if (this.state.status !== currentState.status) {
                this.setState({
                    status: currentState.status
                })
            }

            if (this.state.result !== currentState.result) {
                this.setState({
                    result: currentState.result
                })
            }

        })
    }

    componentWillUnmount() {
        this.unsub()
    }

    private toggleFold(event: React.MouseEvent<HTMLImageElement, MouseEvent>) {
        let icon = event.currentTarget
        icon.classList.toggle("unfold")
        let job = icon.parentElement?.parentElement?.parentElement as HTMLDivElement
        let body = job.getElementsByClassName("body")[0]
        body.classList.toggle("hidden")
    }

    private copyToClipBoard(event: React.MouseEvent<HTMLDivElement, MouseEvent>) {
        let textarea = document.createElement("textarea")
        textarea.innerText = this.state.result
        event.currentTarget.appendChild(textarea)
        textarea.focus()
        textarea.select()
        document.execCommand("copy")
        event.currentTarget.removeChild(textarea)
        alert("Copied to clipboard")
    }

    private delete(event: React.MouseEvent<HTMLDivElement, MouseEvent>) {
        new ApiClient().DeleteJob(this.state.id).then(job => {
            console.log(job)
            if (job.id !== null || job.id === undefined) {
                JobStore.getStore().dispatch({
                    type: "delete",
                    job: job,
                })
            }
        })
    }
}