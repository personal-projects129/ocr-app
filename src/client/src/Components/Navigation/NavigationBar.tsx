import { Component } from "react";
import '../../css/Components/Navigation/NavigationBar.css'
import search from "../../assets/icons/search.png";
import Auth from "../../Api/Auth/Auth";
import JobStore, { IJob } from "../../Stores/JobStore";

interface NavigationBarState {
    AvatarSrc: string
}
export default class NavigationBar extends Component<any, NavigationBarState> {

    private avatarSrc: string;
    constructor(props: any) {
        super(props)
        this.avatarSrc = Auth.GetInstance().GetBearer().User.Picture
    }
    render() {
        return (
            <nav className="nav">
                <div className="header">OCR App</div>
                <div className="toolbar">
                    <div className="searchbar">
                        <input type="text" placeholder="Search..." onChange={this.search.bind(this)}/>
                        <img src={search}/>
                    </div>
                    <div className="avatar" onClick={this.popSubmenu.bind(this)}>
                        <img src={this.avatarSrc}/>
                    </div>
                    <div id="nav-submenu" className="avatar-submenu hidden">
                        <div className="action" onClick={this.logout.bind(this)}>Logout</div>
                    </div>
                </div>
            </nav>
        )
    }


    private popSubmenu(event: React.MouseEvent<HTMLDivElement, MouseEvent>) {
        document.getElementById("nav-submenu")?.classList.toggle("hidden")
    }

    private logout() {
        fetch('/api/auth/web/bearer', {method: "delete"}).then(() => {
            window.location.href = "/login"
        })
    }

    private search(event: React.ChangeEvent<HTMLInputElement>) {    
        JobStore.getStore().dispatch({
            type: "search",
            searchString: event.currentTarget.value,
            job: {} as IJob
        })
    }
}