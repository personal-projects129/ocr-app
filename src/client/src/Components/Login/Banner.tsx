import { Component } from "react";
import '../../css/Components/Login/Banner.css';
import gsap, { Cubic } from 'gsap';
import gitlabIcon from '../../assets/icons/gitlab-icon.png';

interface BannerProps {
    children?: any;
}

export default class Banner extends Component<BannerProps> {
    render() {
        return (
            <div className="login-banner-shadow">
                <div className="login-banner">
                    <div className="header">
                        <a href="https://gitlab.com/gtrmergillazildjianl/ocr-app.git" className="gitlab-logo">
                            <img src={gitlabIcon} alt="gitlab-icon"/>
                            <span>GitLab</span>
                        </a>
                    </div>
                    <div className="content">
                        <h1 className="headline">Simple OCR App</h1>
                        <p className="sub-headline">Extract searchable text from images for free</p>
                    </div>
                    <div className="footer">
                        <span>Copyright © 2021 by ageekhub.com</span>
                    </div>
                </div>
            </div>
        );
    }

    componentDidMount() {
        let tl = gsap.timeline();
        
        if (window.innerWidth < 1280) {
            tl.from('div.login-banner', {
                padding: 0,
                height: 0,
                opacity: 0,
                duration: 1.0,
                ease: Cubic.easeOut
            })
        } else {
            tl.from('div.login-banner', {
                padding: 0,
                width: 0,
                opacity: 0,
                duration: 1.0,
                ease: Cubic.easeOut
            })
        }
        tl.set('div.login-banner', {
            clearProps: 'all'
        })
        tl.from('div.header, div.footer', {
            opacity: 0,
            ease: Cubic.easeOut,
            duration: 0.5
        }).set('div.header, div.footer', {clearProps: 'all'})
        .from('div.content > *', {
            opacity: 0,
            stagger: 1,
            ease: Cubic.easeOut,
            duration: 0.5
        }).set('div.content > *', {clearProps: 'all'});
        tl.play()   
    }

}